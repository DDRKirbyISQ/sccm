The Super Crayola Crew Mod is an attempt to balance the gameplay of SSBM to our liking, using ISO hacking.

Note, when comparisons are given, they are comparisons to the OLD, original characters.
SCCM's base is v1.00 of SSBM (NTSC).  One notable version difference is that v1.00 allows Flame-cancelling with Bowser.



CURRENT COMPLETE SET OF CHANGES:
Miscellaneous:
-Added a .dol hack to enable taunt cancelling
-Added Magus's .dol hacks to enable C-Sticking in 1P modes and replace Tournament with Debug menu.

Bowser:
Bowser is now much faster.  His dashdance is a bit better, but most notably his jump and landing are a lot faster, so his wavedash is actually useable now.  In addition, all of his aerial attacks are a lot faster, so he can combo the heck out of you.  His dair has been changed to a single-hit spike like falco's, and his sideb has better range.  His fthrow also serves as a viable kill move at higher percents.
-doubled walk initial velocity
-doubled walk accel
-increased walk max velocity to that of falcon/peach
-dash initial velocity up 50% to that of doc/gaw
-stopturn decel up by 50% to that of doc/gaw
-jump startup frames down to 5 like falco
-jump h initial velocity up to 1.2 like link/roy
-normal landing lag down to 4 like most chars
-lag on all aerial attacks down to 25 frames (from 30/35/40)
-dash attack IASA changed to 38 (was 53), so dash attacks effectively end 15 frames earlier now
-dsmash IASA changed to 53 (was 68), so dsmashes effectively end 15 frames earlier now
-dair changed to a single-hit move
-dair air hit angle changed from 80 to 290 (is now a non-meteor-cancaellable spike)
-dair air hit base knockback increased from 0 to 20
-dair damage increased from 3 to 12
-dair air hit weight dependent set knockback changed to 0 (was 20), so this is now a non-fixed knockback hit, with the same knockback as falco's dair
-dair air hit sfx changed to that of dair landing hit sfx
-added electric effect to dair air and landing hits (increases hitstun to 1.5x)
-upb ground turning speed increased from 0.15 to 1.0
-downb descent speed tripled from 7.5 to 22.5
-sideb grab hitbox x offsets to 3000 (was 2400 for both)
-sideb grab hitbox sizes to 3000 (was 600, 1400)
-fthrow knockback growth doubled from 50 to 100
-added fire effect to fthrow

Captain Falcon:
Falcon is already perfectly fine, but I wanted to make moonwalking a bit more fun, so I increased its effectiveness.  I also made his walljump go vertically a little more so scarjumping is a lot easier.
-stopturn deceleration increased to 3.75 times original value, to make moonwalking and associated techs (stickywalk, boost) better.  As a side effect, you can turn around while running much faster now.
-walljump v velocity increased, to make scarjumping easier
-fair now has "CHYES!" sfx instead of the normal attack sound
-fair now makes a baseball bat ping sfx when it connects

Donkey Kong:
I don't know much about DK other than his shield sucks, so I fixed that for him.  I also made his fair and dair a bit faster, especially his fair since I feel like it sucks as a move so much that it can afford to be made comboable.  His normal land lag is also down to match most other characters.
-shield size increased by 25%
-fair landing lag down from 30 to 20 (same as nair).
-dair landing lag down from 31 to 25 (same as uair).
-normal land lag down from 5 to 4 (now matches most characters)

Dr. Mario:
No changes.  Doc is perfect!

Falco:
No changes.  Falco is perfect!

Fox:
No changes.  Fox has been, and always will be, Fox.

Game & Watch:
G&W's shield is pitiful, so I fixed that, and also made his weight a little higher so he doesn't die quite as easily now.  I also decreased the lag on bair and uair, made bair into a single-hit marth-like attack, and made uairs easier to connect with, so now they're hopefully useful now.  His dair is now easier to spike with, and judgment hammer has gotten a lot of buffs.  I also finally fixed his spot-dodge and rolls to make them not suck.
-shield size increased by 50%
-weight increased from 60 to 70 (now equal to kirby)
-decreased landing lag on bair from 18 to 12 frames (like an l-cancelled fair now)
-decreased landing lag on uair from 15 to 12 frames (same)
-pan speed increased so you can rapid-fire sausages now, and panspikes come out faster
-dsmash sourspots now act like sweetspots
-dair now always spikes on the first frame, regardless of where it hits (though the hitbox is now the key and not the hilt, so the spike hitbox will miss if they're too high)
-bair changed to a single hit attack (although there's still also a weak hitbox on landing)
-bair sfx changed to that of marth sweetspotted ftilt
-bair angle, damage, base knockback, and knockback growth changed to match that of marth's sweetspotted fair
-doubled sizes of uair hitboxes
-judgment 2: changed element to poison/flower, so it does a little damage over time with a flower
-judgment 3: increased shield damage from 20 to 124; should break shields instantly now
-judgment 4: changed sfx to that of marth's sweetspotted ftilt
-judgment 4: increased damage from 8 to 20
-judgment 5: decreased delay between hits from 3 frames to 1 frame, increased number of hits from 4 to 8
-judgment 7: changed element to disable so it stuns the enemy (like mewtwo's disable or a shield break) if they're on the ground
-judgment 9: doubled base knockback
-spot dodge invincibility changed from 2-12 to 2-25 (out of 32)

Ganondorf:
I don't know that much about ganon, but I think he's mostly fine...but I made his jump and landing a little faster so it doesn't feel as awkward now.  In addition, I made his downb faster and his warlock punch as a really weird property that lets you actually use it to edgeguard...sort of.
-jump startup lag down from 6 to 5 (now same as falco, peach)
-normal land lag down from 5 to 4 (now matches most characters)
-warlock punch: changed momentum forward in air from 0.8 to -1.0E-6 and changed momentum from holding up from 0.92 to 1.36.  Also changed the frame timings of timers from 73 and 94 to 115 and 116.  This has the effect of holding you stationary during the punch and also pushing you back horizontally when you air WP.  If you also hold down, you'll be pushed upwards, allowing you to edgeguard with it.
-downb ground-ground end lag modifier changed from 0.8 to 2.0, making grounded downb's have less lag
-downb air-ground end lag modifier changed from 0.8 to 2.5, making aerial downb's have much less lag upon landing
-uptilt hitbox sizes increased from 1800 to 7200

Ice Climbers:
Ice Climbers are crazy.  They're fine.

Jigglypuff:
I can't figure out how to fix Jigglypuff so she's not broken and/or annoying, so no changes for now.  I just don't know.  I had the idea of making her fall faster but tweaking her jumps to compensate, but turns out I can't tweak her jumps.  So I really have no idea what to do about her that maintains her playstyle yet doesn't make her stupidly floaty.

Kirby:
Kirby is beastly now.  He runs faster and has a nice wavedash, and now has aerial mobility that rivals that of jiggs.  He's a lot faster and more maneuverable overall.  In addition, his nair is now a useful sex-kick style attack instead of the weak move it was before.  As a slight tradeoff, he only has 4 jumps now, though his increased aerial mobility is more than enough to make up for it.
-number of jumps down to 4 (from 6)
-dash initial velocity increased to that of donkey kong (faster than mario/marth), up by ~15%.
-friction decreased by 25%, down to that of mario/marth (should make wavedashing longer).
-run speed up to that of dk/roy (up ~15%).
-air accel increased by 100% (almost comparable to jigglypuff).
-max aerial h velocity up ~100%, to that of jiggs.
-shorthop v initial velocity down 20%.
-fair, dair landing lag down from 20 to 15 (same as bair, uair, nair).
-dash attack second hitbox now ends on frame 30 (was 44) and IASA changed to 30 (was 60), so dash attacks effectively end 30 frames earlier now
-nair hitbox now comes out on frame 5 (was 10)
-nair hit sfx changed to same as fox's sex kick instead of the high-pitched "thwack"
-dthrow base knockback increased from 40 to 60
-uthrow base knockback increased from 70 to 100
-uthrow knockback growth increased from 70 to 80
-dtilt angle changed from 20 to 80 for comboing
-dtilt IASA changed from frame 30 to 20, so dtilts effectively end 10 frames earlier now
-sideb sweetspot knockback growth increased from 76 to 90
-sideb sourspot knockback growth increased from 50 to 90 (same as new sweetspot)
-sideb sourspot base knockback increased from 50 to 65 (same as sweetspot)
-sideb sourspot sfx changed to same as sweetspot sfx
-aerial hammer vertical momentum decreased from 1.5 to 1.0
-aerial hammer landing lag decreased from 16 frames to 10 frames
-stone hitbox angle changed from 70 to 290 - is now a spike
-minimum stone duration decreased from 18 frames to 1 frame

Link:
Link's movement always felt weird because his dash was so slow and his wavedash was so awkward, so I made his jump slightly faster, increased his run speed, and gave him a better wavedash.
-jump startup lag down from 6 to 5 (now same as falco, peach)
-friction down 40% to that of most chars (mario).  This also means shine combos won't work as well.
-run speed up very slightly, to that of ganondorf, slightly faster than luigi.

Luigi:
Luigi is mostly fine, but I wanted to give him a little more aerial maneuverability so that his air attacks weren't always so...weird...because you can never move anywhere in the air with regular luigi.  I also made his misfire go off more often, for extra fun!
-jump h initial velocity up 66% (between that of marth and ness)
-misfire ratio increased from 1 in 8 to 1 in 4

Mario:
Mario is perfectly fine!

Marth:
Marth is perfectly fine!

Mewtwo:
Mewtwo has always been way underpowered in my book, so he's gotten a whole crapton of buffs.  He basically moves a hell of a lot faster now, though the majority of his moves are still the same.  One notable difference is his fair, which now acts as a sex kick-style attack.  In addition, his moonwalk is better, and he's actually SMALLER, which is good for him because it makes him less of a target.  He's a bit less floaty which is good for comboing, and his nair and bair are faster.  His shield is better too, and he's even gotten pichu's special 2-frame empty jump landing lag.  He generally can't be as aggressive with edgeguarding, however, since he falls faster now.
-size decreased to 90% of original (hitboxes should scale)
-run speed up to that of dk/roy (up ~15%).
-stopturn deceleration up from 0 to 0.05.  Should theoretically make turning around faster and moonwalking moves better, but this will have to be tested.
-increased dash initial velocity to that of donkey kong (faster than mario/marth), up by ~15%.
-dair and bair landing lag decreased from 28 to 20 (same as uair).
-dash initial velocity up to that of Marth.
-jump h initial velocity up slightly to that of Ganondorf.
-gravity increased ~15% to that of Mario.
-nair, bair landing lag down to 20 frames (like uair).
-normal land lag down from 4 to 2 (now matches pichu)
-shield size increased by 25%
-weight increased ~15% to that of Mario
-jump startup lag down from 5 to 4 (now matches most characters)
-fair changed to one hitbox, same as ending (big) hitbox of nair but 75% the size
-fair hitbox now stays out like a sex kick
-fair base knockback decreased from 40 to 10
-fair angle changed from 84 to 80
-fair damage reduced from 14 to 10
-dair angle changed from 270 to 290, this might make it non-meteor-cancellable now
-spot dodge invincibility changed from 2-22 to 2-30 (out of 37)
-dash attack IASA changed to 28 (was 38), so dash attacks effectively end 10 frames earlier now
-shadow ball projectile lifetime up from 70 to 140
-shadow ball base size up from 0.7 to 1.0
-shadow ball full size up from 2.6 to 4.0
-shadow ball uncharged speed down from 1.6 to 1.25
-shadow ball charged speed up from 1.7 to 2.5
Ness:
I don't really know what needs to be fixed with ness really, but his ground speed seems really slow, so I made him run faster.  I also made his nair slightly faster, which should theoretically help with combos.  As an experiment, I also tried to make his YYG easier to execute.
-dash initial velocity up ~25% to that of donkey kong (faster than mario/marth)
-run speed up ~15% to that of roy, young link, yoshi
-nair landing lag down from 22 to 18 (same as fair, bair, uair)
-second and third hitboxes of usmash are delayed much longer, so that they don't even come out when you're trying to YYG--this makes YYG much, much easier to activate (though you still need to time the charge right)
-made first hitbox of usmash twice as large to make YYG easier to activate
-removed weak hitbox of dsmash
-changed dsmash angle from 70 degrees to 32 degrees (same as falcon's knee)
-reduced knockback growth of all dsmash hits to a consistent 50 (initial backswing was originally 70)
-psi magnet now releases instantly instead of holding for 30 frames after b is released

Peach:
I don't understand Peach well at all but I think she's probably just fine.

Pichu:
I think pichu should be even faster, since he's weaker and lighter than fox and falcon, without a shine or knee.  So he now has a better wavedash, runs faster, has a faster shorthop, and has much faster attacks.  His weight is increased a little bit, but he's still pretty fragile.  He also has more combo options out of dsmashes and dtilts, uairs, and even dash attacks, and hit fthrow sets up for edgeguarding.  I wish I could make agility faster.  Unfortunately the new run speed makes agility useless outside of recovery.
-friction down 40% to that of most chars (mario).
-dash initial velocity up ~5% to that of fox/falco.
-run speed up ~25% to that of Fox.
-weight increased from 55 to 65 (between jigglypuff and kirby).
-shorthop v initial velocity down slightly to that of Peach.
-dair landing lag down from 26 to 18 (now matches bair, uair).
-dsmash IASA changed from frame 51 to 25, so dsmashes effectively end 26 frames earlier now
-dsmash angle changed from 160 to 45 (sakurai angle) so it no longer sends backwards and can be used to combo instead
-increased farthest fsmash hitbox size by 25% (was 1363, now 1704)
-dtilt angle changed from 35 to 80 for comboing
-dash attack IASA changed from frame 50 to 30, so dash attacks effectively end 20 frames earlier now
-uair base knockback reduced from 100 to 60, for comboing
-increased base knockback of fthrow 33% from 45 to 60

Pikachu:
I need more experience with Pikachu, but I think he's probably fine.

Roy:
Roy is mostly fine I think, but I made his bair and dair faster, and his spikes are consistent.  I also made his counter noticeably easier to pull off, which should be fun.
-bair landing lag down from 24 to 20 (now matches fair, nair).
-dair landing lag down from 32 to 20 (now matches fair, nair).
-all hitboxes of third high sideb now spike with angle 280
-all hitboxes of dair now spike with angle 290
-downb timing changed to match that of marth (counters on frames 5-29 instead of 8-20)

Samus:
I don't understand Samus at all, so I didn't touch her.

Sheik:
I don't really like Shiek's playstyle, but she doesn't need any changes.

Yoshi:
I don't understand Yoshi enough to make any changes.  I wish I could let him jump out of shield.

Young Link:
Young Link is totally fine to me.

Zelda:
Zelda's movement always felt pretty restrictive, so I gave her a better wavedash and a better run.  She also jumps faster, so those fairs and bairs will come out -slightly- earlier.  Her dair isn't as laggy and serves as an actual semi-powerful spike now, so it can also potentially open up combos and be used for edgeguarding.
-friction down 65% to just above that of Ice Climbers.  This also means shine combos won't work as well.
-dash initial velocity up ~33% to that of mario.
-jump startup lag down from 6 to 4 frames (same as mario).
-run speed up ~25% to that of Samus.
-dair landing lag down from 24 to 18 (same as nair, fair, bair).
-dair now has electric hit effect
-dair hit sfx is now the same as fair/bair
-dair first hitbox base knockback changed from 5 to 20
-dair second hitbox base knockback changed from 0 to 20
-dair first hitbox knockback growth changed from 100 to 80
-(dair second hitbox knockback growth remains at 80)
-dair first hitbox damage changed from 8 to 10
-dair second hitbox damage changed from 7 to 10
-forwardb now ignites instantly instead of being delayed 22 frames



CHANGELOG:
v2.01 (7/28/12)
===============
Miscellaneous:
-Added a .dol hack to enable taunt cancelling

Kirby:
-aerial hammer vertical momentum decreased from 1.5 to 1.0
-aerial hammer landing lag decreased from 16 frames to 10 frames
-stone hitbox angle changed from 70 to 290 - is now a spike
-minimum stone duration decreased from 18 frames to 1 frame

Luigi:
-misfire ratio increased from 1 in 8 to 1 in 4

Mewtwo:
-shadow ball projectile lifetime up from 70 to 140
-shadow ball base size up from 0.7 to 1.0
-shadow ball full size up from 2.6 to 4.0
-shadow ball uncharged speed down from 1.6 to 1.25
-shadow ball charged speed up from 1.7 to 2.5

v2.00 (3/29/12)
===============
Miscellaneous:
-Added Magus's .dol hacks to enable C-Sticking in 1P modes and replace Tournament with Debug menu.

Captain Falcon:
-fair now has "CHYES!" sfx instead of the normal attack sound
-fair now makes a baseball bat ping sfx when it connects
-stopturn decel increased even more to 0.3, now 3.75 times original value for even better moonwalking

Ness:
-second and third hitboxes of usmash are delayed much longer, so that they don't even come out when you're trying to YYG--this makes YYG much, much easier to activate (though you still need to time the charge right)
-made first hitbox of usmash twice as large to make YYG easier to activate
-removed weak hitbox of dsmash
-changed dsmash angle from 70 degrees to 32 degrees (same as falcon's knee)
-reduced knockback growth of all dsmash hits to a consistent 50 (initial backswing was originally 70)
-psi magnet now releases instantly instead of holding for 30 frames after b is released

Pichu:
-dsmash IASA changed from frame 51 to 25, so dsmashes effectively end 26 frames earlier now
-dsmash angle changed from 160 to 45 (sakurai angle) so it no longer sends backwards and can be used to combo instead
-increased farthest fsmash hitbox size by 25% (was 1363, now 1704)
-dtilt angle changed from 35 to 80 for comboing
-dash attack IASA changed from frame 50 to 30, so dash attacks effectively end 20 frames earlier now
-uair base knockback reduced from 100 to 60, for comboing
-increased base knockback of fthrow 33% from 45 to 60

Kirby:
-dash attack second hitbox now ends on frame 30 (was 44) and IASA changed to 30 (was 60), so dash attacks effectively end 30 frames earlier now
-nair hitbox now comes out on frame 5 (was 10)
-nair hit sfx changed to same as fox's sex kick instead of the high-pitched "thwack"
-nair landing lag back to original 15 frames (previously was 12)
-dthrow base knockback increased from 40 to 60
-uthrow base knockback increased from 70 to 100
-uthrow knockback growth increased from 70 to 80
-dtilt angle changed from 20 to 80 for comboing
-dtilt IASA changed from frame 30 to 20, so dtilts effectively end 10 frames earlier now
-sideb sweetspot knockback growth increased from 76 to 90
-sideb sourspot knockback growth increased from 50 to 90 (same as new sweetspot)
-sideb sourspot base knockback increased from 50 to 65 (same as sweetspot)
-sideb sourspot sfx changed to same as sweetspot sfx

Mewtwo:
-fair changed to one hitbox, same as ending (big) hitbox of nair but 75% the size
-fair hitbox now stays out like a sex kick
-fair base knockback decreased from 40 to 10
-fair angle changed from 84 to 80
-fair damage reduced from 14 to 10
-dair angle changed from 270 to 290, this might make it non-meteor-cancellable now
-spot dodge invincibility changed from 2-22 to 2-30 (out of 37)
-dash attack IASA changed to 28 (was 38), so dash attacks effectively end 10 frames earlier now

Bowser:
-dash attack IASA changed to 38 (was 53), so dash attacks effectively end 15 frames earlier now
-dsmash IASA changed to 53 (was 68), so dsmashes effectively end 15 frames earlier now
-dair changed to a single-hit move
-dair air hit angle changed from 80 to 290 (is now a non-meteor-cancaellable spike)
-dair air hit base knockback increased from 0 to 20
-dair damage increased from 3 to 12
-dair air hit weight dependent set knockback changed to 0 (was 20), so this is now a non-fixed knockback hit, with the same knockback as falco's dair
-dair air hit sfx changed to that of dair landing hit sfx
-added electric effect to dair air and landing hits (increases hitstun to 1.5x)
-upb ground turning speed increased from 0.15 to 1.0
-downb descent speed tripled from 7.5 to 22.5
-sideb grab hitbox x offsets to 3000 (was 2400 for both)
-sideb grab hitbox sizes to 3000 (was 600, 1400)
-fthrow knockback growth doubled from 50 to 100
-added fire effect to fthrow

Roy:
-all hitboxes of third high sideb now spike with angle 280
-all hitboxes of dair now spike with angle 290
-downb timing changed to match that of marth (counters on frames 5-29 instead of 8-20)

Ganondorf:
-warlock punch: changed momentum forward in air from 0.8 to -1.0E-6 and changed momentum from holding up from 0.92 to 1.36.  Also changed the frame timings of timers from 73 and 94 to 115 and 116.  This has the effect of holding you stationary during the punch and also pushing you back horizontally when you air WP.  If you also hold down, you'll be pushed upwards, allowing you to edgeguard with it.
-downb ground-ground end lag modifier changed from 0.8 to 2.0, making grounded downb's have less lag
-downb air-ground end lag modifier changed from 0.8 to 2.5, making aerial downb's have much less lag upon landing
-uptilt hitbox sizes increased from 1800 to 7200

Zelda:
-dair now has electric hit effect
-dair hit sfx is now the same as fair/bair
-dair first hitbox base knockback changed from 5 to 20
-dair second hitbox base knockback changed from 0 to 20
-dair first hitbox knockback growth changed from 100 to 80
-(dair second hitbox knockback growth remains at 80)
-dair first hitbox damage changed from 8 to 10
-dair second hitbox damage changed from 7 to 10
-forwardb now ignites instantly instead of being delayed 22 frames

Game & Watch:
-pan speed increased so you can rapid-fire sausages now, and panspikes come out faster
-dsmash sourspots now act like sweetspots
-dair now always spikes on the first frame, regardless of where it hits (though the hitbox is now the key and not the hilt, so the spike hitbox will miss if they're too high)
-bair changed to a single hit attack (although there's still also a weak hitbox on landing)
-bair sfx changed to that of marth sweetspotted ftilt
-bair angle, damage, base knockback, and knockback growth changed to match that of marth's sweetspotted fair
-doubled sizes of uair hitboxes
-judgment 2: changed element to poison/flower, so it does a little damage over time with a flower
-judgment 3: increased shield damage from 20 to 124; should break shields instantly now
-judgment 4: changed sfx to that of marth's sweetspotted ftilt
-judgment 4: increased damage from 8 to 20
-judgment 5: decreased delay between hits from 3 frames to 1 frame, increased number of hits from 4 to 8
-judgment 7: changed element to disable so it stuns the enemy (like mewtwo's disable or a shield break) if they're on the ground
-judgment 9: doubled base knockback
-spot dodge invincibility changed from 2-12 to 2-25 (out of 32)

v1.03 (11/23/11)
================
Kirby:
-nair landing lag down from 15 to 12 (now matches pichu's nair).

Zelda:
-dair landing lag down from 24 to 18 (same as nair, fair, bair).

v1.02 (11/23/11)
================
G&W:
-shield size increased another 25%, now 150% original value

Mewtwo:
-normal land lag down from 4 to 2 (now matches pichu)
-shield size increased by 25%
-weight increased ~15% to that of Mario
-jump startup lag down from 5 to 4 (now matches most characters)

Ganondorf:
-normal land lag down from 5 to 4 (now matches most characters)

Ness:
-dash initial velocity up ~25% to that of donkey kong (faster than mario/marth)
-run speed up ~15% to that of roy, young link, yoshi

DK:
-shield size increased another 15% (now 125% of original)
-normal land lag down from 5 to 4 (now matches most characters)

v1.01 (9/21/11)
===============
Kirby:
-number of jumps down to 4 (from 6)

Luigi:
-jump h initial velocity toned down to 66% increase over original (was 100%)

Mewtwo:
-dash initial velocity up again to that of Marth
-jump h initial velocity up slightly to that of Ganondorf
-gravity increased ~15% to that of Mario
-nair, bair landing lag down to 20 frames (like uair)

Pichu:
-run speed up 10% to that of Fox
-shorthop v initial velocity down slightly to that of Peach
-dair landing lag down from 26 to 18 (now matches bair, uair)

Roy:
-bair landing lag down from 24 to 20 (now matches fair, nair)
-dair landing lag down from 32 to 20 (now matches fair, nair)

Zelda:
-run speed up ~25% to that of Samus
-friction down another 25%, to just above that of Ice Climbers

Link:
-jump startup lag down from 6 to 5 (now same as falco, peach)

Ganondorf:
-jump startup lag down from 6 to 5 (now same as falco, peach)

v1.00 (9/1/11)
==============
*First test release!*

Bowser:
-doubled walk initial velocity
-doubled walk accel
-increased walk max velocity to that of falcon/peach
-dash initial velocity up 50% to that of doc/gaw
-stopturn decel up by  50% to that of doc/gaw
-jump startup frames down to 5 like falco
-jump h initial velocity up to 1.2 like link/roy
-normal landing lag down to 4 like most chars
-lag on all aerial attacks down to 25 frames (from 30/35/40)

Falcon:
-stopturn deceleration increased by 50%, to make moonwalking and associated techs (stickywalk, boost) better.  As a side effect, you can turn around while running much faster now.
-walljump v velocity increased, to make scarjumping easier

G&W:
-shield size increased by 25%
-weight increased from 60 to 70 (now equal to kirby)
-decreased landing lag on bair from 18 to 12 frames (like an l-cancelled fair now)
-decreased landing lag on uair from 15 to 12 frames (same)

Mewtwo:
-size decreased to 90% of original (hitboxes should scale)
-run speed up to that of dk/roy (up ~15%).
-stopturn deceleration up from 0 to 0.05.  Should theoretically make turning around faster and moonwalking moves better, but this will have to be tested.
-increased dash initial velocity to that of donkey kong (faster than mario/marth), up by ~15%.
-dair and bair landing lag decreased from 28 to 20 (same as uair).

DK:
-shield size increased by 10%
-fair landing lag down from 30 to 20 (same as nair).
-dair landing lag down from 31 to 25 (same as uair).

Kirby:
-dash initial velocity increased to that of donkey kong (faster than mario/marth), up by ~15%.
-friction decreased by 25%, down to that of mario/marth (should make wavedashing longer).
-run speed up to that of dk/roy (up ~15%).
-air accel increased by 100% (almost comparable to jigglypuff).
-max aerial h velocity up ~100%, to that of jiggs.
-shorthop v initial velocity down 20%.
-fair, dair landing lag down from 20 to 15 (same as bair, uair, nair).

Luigi:
-jump h initial velocity up 100% to that of mario.

Zelda:
-friction down 40% to that of most chars (mario).  This also means shine combos won't work as well.
-dash initial velocity up ~33% to that of mario.
-jump startup lag down from 6 to 4 frames (same as mario).

Pichu:
-friction down 40% to that of most chars (mario).
-dash initial velocity up ~5% to that of fox/falco.
-run speed up ~15% (between marth and fox).
-weight increased from 55 to 65 (between jigglypuff and kirby).

Ness:
-nair landing lag down from 22 to 18 (same as fair, bair, uair)

Link:
-friction down 40% to that of most chars (mario).  This also means shine combos won't work as well.
-run speed up very slightly, to that of ganondorf, slightly faster than luigi.